
class Scrabble:
    SCRABBLES_SCORES = [(1, "E A O I N R T L S U"), (2, "D G"), (3, "B C M P"),
                        (4, "F H V W Y"), (5, "K"), (8, "J X"), (10, "Q Z")]
    LETTER_SCORES = {letter: score for score, letters in SCRABBLES_SCORES for letter in letters.split()}

    point = 0
    word = "txt"

    def __init__(self, word):
        self.word = word.upper()
        for x in self.word:
            self.point = int(self.point + self.LETTER_SCORES[x])


def open_file():
    array_word = []
    with open("dictionary.txt", "r") as file:
        text = file.read().split('\n')
    for x in text:
        array_word.append(Scrabble(x))
    return array_word


def best_score(array_word):
    high_score = 0
    best_word = 0
    for x in range(len(array_word)):
        if array_word[x].point > high_score:
            high_score = array_word[x].point
            best_word = x
    return best_word


def word_value(value, array_word):
    correct_word_index = None
    for x in range(len(array_word)):
        if array_word[x].point == value:
            correct_word_index = x
    return correct_word_index
