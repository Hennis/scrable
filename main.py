import logic


def chose(options, word_array):
    if options == "1":
        index = logic.best_score(word_array)
        print("The highest result word: ", word_array[index].word)
        print("Points od the word: ", word_array[index].point)
    if options == "2":
        new_word = input("Write your word: ")
        new_word = logic.Scrabble(new_word)
        print("Score of your word: ", new_word.point)
    if options == "3":
        value = int(input("Write value: "))
        correct_word_index = logic.word_value(value, word_array)
        if correct_word_index is not None:
            print("Word with score ", value, " is: ", word_array[correct_word_index].word)


def run():
    word_array = logic.open_file()

    print("1) The highest result from the file")
    print("2) The result for the word from the command line")
    print("3) Returns the word with the given value")

    chose_option = input("Write the number of the selected option: ")
    chose(chose_option, word_array)


if __name__ == '__main__':
    run()

