import unittest
import logic
import main


class Test(unittest.TestCase):
    def test_scrabble_word(self):
        self.assertEqual(logic.Scrabble("quiz").word, "QUIZ")

    def test_scrabble_point(self):
        self.assertEqual(logic.Scrabble("quiz").point, 22)

    def test_best_score(self):
        test_array = [logic.Scrabble("how"), logic.Scrabble("on"), logic.Scrabble("dog")]
        self.assertEqual(logic.best_score(test_array), 0)

    def test_word_value(self):
        test_array = [logic.Scrabble("lost"), logic.Scrabble("how"), logic.Scrabble("on"), logic.Scrabble("dog")]
        self.assertEqual(logic.word_value(4, test_array), 0)


